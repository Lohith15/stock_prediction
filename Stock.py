# import yfinance as yf
# from neuralprophet import NeuralProphet
# import streamlit as st
# from datetime import date

# st.title('Stock Price Prediction')
# st.write('Enter the stock symbol and the number of days to predict')

# symbol = st.text_input('Stock Symbol', value='AAPL')
# n_days = st.slider('Days to Predict', min_value=1, max_value=365, value=30, step=1)

# df=yf.download(symbol, "2019-01-01", date.today().strftime("%Y-%m-%d"))
# df.reset_index(inplace=True)
# df = df.dropna()
# print(df)
# df = df[['Date','Close']]
# df= df.rename(columns={"Date": "ds", "Close": "y"})
# print(df)

# model = NeuralProphet()
# model.fit(df, freq='D')
# #model.fit(df)

# future = model.make_future_dataframe(df,periods=n_days)
# forecast = model.predict(future)
# print(forecast)

# print(forecast[['ds', 'yhat']])
# st.write('Predicted stock prices:')
# st.line_chart(forecast[['ds', 'yhat']])

import yfinance as yf
from neuralprophet import NeuralProphet
import streamlit as st
from datetime import date

st.title('Stock Price Prediction')
st.write('Enter the stock symbol and the number of days to predict')

symbol = st.text_input('Stock Symbol', value='AAPL')
n_days = st.slider('Days to Predict', min_value=1, max_value=365, value=30, step=1)

df=yf.download(symbol, "2015-01-01", date.today().strftime("%Y-%m-%d"))
df.reset_index(inplace=True)
df = df.dropna()

st.subheader('Raw data')
st.write(df.tail())

df = df[['Date','Close']]
df= df.rename(columns={"Date": "ds", "Close": "y"})
print(df)

model = NeuralProphet()
model.fit(df, freq='D')
#model.fit(df)

future = model.make_future_dataframe(df,periods=n_days)
forecast = model.predict(future)
st.write(forecast.tail())

# print(forecast[['ds', 'yhat']])
st.write('Predicted stock prices:')
st.line_chart(forecast[['ds', 'yhat1']])